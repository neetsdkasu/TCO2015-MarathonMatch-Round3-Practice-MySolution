Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic

Public Class PegJumping

    Dim dt() As Integer = { -1, 0, 1, 0, -1 }
    Dim dts() As String = { "U", "R", "D", "L" }

    Dim rand As New Random(19831983)

    Dim N As Integer
    Dim pegValue() As Integer
    Dim grid() As Integer

    Public Function getMoves(pegValue() As Integer, board() As String) As String()
        Dim startTime As Integer = Environment.TickCount
        Dim limitTime As Integer = startTime + 9000

        N = board.Length
        Me.pegValue = pegValue
        ReDim grid(N * N - 1)
        For row As Integer = 0 To N - 1
            For col As Integer = 0 To N - 1
                If board(row)(col) = "."c Then
                    grid(row * N + col) = -1
                Else
                    grid(row * N + col) = Asc(board(row)(col)) - Asc("0")
                End If
            Next col
        Next row

        Dim ret As New List(Of String)()

        Dim score As Long = Search(ret, TryCast(grid.Clone(), Integer()), 0, 1)

        For di0 As Integer = 0 To 3
            For di1 As Integer = 0 To 3
                Dim tmp As New List(Of String)()
                Dim grid0() As Integer = TryCast(grid.Clone(), Integer())
                Dim tmpScore As Long = 0
                tmpScore += Search(tmp, grid0, di0, 2)
                For i As Integer = 1 To 5
                    tmpScore += Search(tmp, grid0, di1, 1)
                Next i
                If tmpScore > score Then
                    score = tmpScore
                    ret = tmp
                End If
            Next di1
        Next di0

        Dim posOrder(N * N - 1) As Integer
        For i As Integer = 0 To UBound(posOrder)
            posOrder(i) = i
        Next i

        Dim time0 As Integer = Environment.TickCount
        Dim time1 As Integer = time0
        Dim interval As Integer = 0

        For cy As Integer = 1 To 10000000
            time0 = time1
            time1 = Environment.TickCount
            interval = Math.Max(interval, time1 - time0)
            If time1 + interval > limitTime Then
                Console.Error.WriteLine("cy: {0}", cy)
                Console.Error.WriteLine("interval: {0} ms", interval)
                Exit For
            End If

            Shuffle(posOrder)
            For di0 As Integer = 0 To 3
                Dim tmp As New List(Of String)()
                Dim grid0() As Integer = TryCast(grid.Clone(), Integer())
                Dim tmpScore As Long = 0
                tmpScore += Search(tmp, grid0, posOrder, di0, 1)
                tmpScore += Search(tmp, grid0, posOrder, di0, 1)
                If tmpScore > score Then
                    score = tmpScore
                    ret = tmp
                End If
            Next di0

            Shuffle(posOrder)
            For di0 As Integer = 0 To 3
                For di1 As Integer = 0 To 3
                    Dim tmp As New List(Of String)()
                    Dim grid0() As Integer = TryCast(grid.Clone(), Integer())
                    Dim tmpScore As Long = 0
                    tmpScore += Search(tmp, grid0, di0, 2)
                    For i As Integer = 1 To 3
                        tmpScore += Search(tmp, grid0, posOrder, di1, 1)
                    Next i
                    If tmpScore > score Then
                        score = tmpScore
                        ret = tmp
                    End If
                Next di1
            Next di0

        Next cy

        Console.Error.WriteLine("sc: {0}", score)

        getMoves = ret.ToArray()

    End Function

    Sub Shuffle(Of T)(arr() As T)
        For i As Integer = UBound(arr) To 1 Step -1
            Dim j As Integer = rand.Next(i + 1)
            Dim tmp As T = arr(i)
            arr(i) = arr(j)
            arr(j) = tmp
        Next i
    End Sub

    Function Search(ret As List(Of String), grid() As Integer, di0 As Integer, stepSize As Integer) As Long
        Dim score As Long = 0

        For row As Integer = 0 To N - 1
            For col As Integer = 0 To N - 1

                If grid(row * N + col) < 0 Then
                    Continue For
                End If

                Dim r0 As Integer = row
                Dim c0 As Integer = col
                Dim s As String = ""

                Dim tmpScore As Long = 0

                For i As Integer = 1 To N

                    Dim moved As Boolean = False

                    For ddi As Integer = 0 To 3 Step stepSize
                        Dim di As Integer = (ddi + di0) Mod 4
                        Dim r1 As Integer = r0 + dt(di)
                        Dim c1 As Integer = c0 + dt(di + 1)
                        If r1 < 0 OrElse c1 < 0 OrElse r1 >= N OrElse c1 >= N Then
                            Continue For
                        End If
                        If grid(r1 * N + c1) < 0 Then
                            Continue For
                        End If
                        Dim r2 As Integer = r1 + dt(di)
                        Dim c2 As Integer = c1 + dt(di + 1)
                        If r2 < 0 OrElse c2 < 0 OrElse r2 >= N OrElse c2 >= N Then
                            Continue For
                        End If
                        If grid(r2 * N + c2) >= 0 Then
                            Continue For
                        End If
                        tmpScore += pegValue(grid(r1 * N + c1))
                        grid(r2 * N + c2) = grid(r0 * N + c0)
                        grid(r0 * N + c0) = -1
                        grid(r1 * N + c1) = -1
                        r0 = r2
                        c0 = c2
                        moved = True
                        s += dts(di)
                        Exit For
                    Next ddi

                    If Not moved Then
                        Exit For
                    End If

                Next i

                If s.Length > 0 Then
                    score += CLng(tmpScore) * CLng(s.Length)
                    ret.Add(CStr(row) + " " + CStr(col) + " " + s)
                End If

            Next col
        Next row

        Search = score
    End Function

    Function Search(ret As List(Of String), grid() As Integer, posOrder() As Integer, di0 As Integer, stepSize As Integer) As Long
        Dim score As Long = 0

        For Each pos As Integer In posOrder

            If grid(pos) < 0 Then
                Continue For
            End If

            Dim row As Integer = pos \ N
            Dim col As Integer = pos Mod N

            Dim r0 As Integer = row
            Dim c0 As Integer = col
            Dim s As String = ""

            Dim tmpScore As Long = 0

            For i As Integer = 1 To N

                Dim moved As Boolean = False

                For ddi As Integer = 0 To 3 Step stepSize
                    Dim di As Integer = (ddi + di0) Mod 4
                    Dim r1 As Integer = r0 + dt(di)
                    Dim c1 As Integer = c0 + dt(di + 1)
                    If r1 < 0 OrElse c1 < 0 OrElse r1 >= N OrElse c1 >= N Then
                        Continue For
                    End If
                    If grid(r1 * N + c1) < 0 Then
                        Continue For
                    End If
                    Dim r2 As Integer = r1 + dt(di)
                    Dim c2 As Integer = c1 + dt(di + 1)
                    If r2 < 0 OrElse c2 < 0 OrElse r2 >= N OrElse c2 >= N Then
                        Continue For
                    End If
                    If grid(r2 * N + c2) >= 0 Then
                        Continue For
                    End If
                    tmpScore += pegValue(grid(r1 * N + c1))
                    grid(r2 * N + c2) = grid(r0 * N + c0)
                    grid(r0 * N + c0) = -1
                    grid(r1 * N + c1) = -1
                    r0 = r2
                    c0 = c2
                    moved = True
                    s += dts(di)
                    Exit For
                Next ddi

                If Not moved Then
                    Exit For
                End If

            Next i

            If s.Length > 0 Then
                score += CLng(tmpScore) * CLng(s.Length)
                ret.Add(CStr(row) + " " + CStr(col) + " " + s)
            End If

        Next pos

        Search = score
    End Function

End Class
