Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic

Module Main

    Function CallGetMoves(_pegJumping As PegJumping) As Integer
        Dim _pegValueSize As Integer = CInt(Console.ReadLine()) - 1
        Dim pegValue(_pegValueSize) As Integer
        For _idx As Integer = 0 To _pegValueSize
            pegValue(_idx) = CInt(Console.ReadLine())
        Next _idx
        Dim _boardSize As Integer = CInt(Console.ReadLine()) - 1
        Dim board(_boardSize) As String
        For _idx As Integer = 0 To _boardSize
            board(_idx) = Console.ReadLine()
        Next _idx

        Dim _result As String() = _pegJumping.getMoves(pegValue, board)

        Console.WriteLine(_result.Length)
        For Each _it As String In _result
            Console.WriteLine(_it)
        Next _it
        Console.Out.Flush()

        Return 0
    End Function

    Sub Main()
        Dim _pegJumping As New PegJumping()

        ' do edit codes if you need

        CallGetMoves(_pegJumping)

    End Sub

End Module
