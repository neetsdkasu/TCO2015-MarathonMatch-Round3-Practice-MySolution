TCO2015 Marathon Match Round 3 (Practice) My Solution
=====================================================


Problem Statement  
https://community.topcoder.com/longcontest/?module=ViewProblemStatement&rd=17124&pm=13843  


Standings  
https://community.topcoder.com/longcontest/?module=ViewStandings&rd=17124  


Submit  
https://community.topcoder.com/longcontest/?module=Submit&compid=48422&rd=17124&cd=15382  


Marathon Match List for Practice  
https://community.topcoder.com/longcontest/?module=ViewPractice  